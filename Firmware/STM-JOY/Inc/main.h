/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LED2_Pin GPIO_PIN_13
#define LED2_GPIO_Port GPIOC
#define OSC32_IN_Pin GPIO_PIN_14
#define OSC32_IN_GPIO_Port GPIOC
#define OSC32_OUT_Pin GPIO_PIN_15
#define OSC32_OUT_GPIO_Port GPIOC
#define J1_X_Pin GPIO_PIN_0
#define J1_X_GPIO_Port GPIOA
#define J1_Y_Pin GPIO_PIN_1
#define J1_Y_GPIO_Port GPIOA
#define J1_Z_Pin GPIO_PIN_2
#define J1_Z_GPIO_Port GPIOA
#define J1_rX_Pin GPIO_PIN_3
#define J1_rX_GPIO_Port GPIOA
#define J1_rY_Pin GPIO_PIN_4
#define J1_rY_GPIO_Port GPIOA
#define J1_Rudder_Pin GPIO_PIN_5
#define J1_Rudder_GPIO_Port GPIOA
#define J1_Throttle_Pin GPIO_PIN_6
#define J1_Throttle_GPIO_Port GPIOA
#define J2_X_Pin GPIO_PIN_7
#define J2_X_GPIO_Port GPIOA
#define J2_Y_Pin GPIO_PIN_0
#define J2_Y_GPIO_Port GPIOB
#define J2_Z_Pin GPIO_PIN_1
#define J2_Z_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define J1_B1_Pin GPIO_PIN_10
#define J1_B1_GPIO_Port GPIOB
#define J1_B2_Pin GPIO_PIN_11
#define J1_B2_GPIO_Port GPIOB
#define J1_B3_Pin GPIO_PIN_12
#define J1_B3_GPIO_Port GPIOB
#define J1_B4_Pin GPIO_PIN_13
#define J1_B4_GPIO_Port GPIOB
#define J1_B5_Pin GPIO_PIN_14
#define J1_B5_GPIO_Port GPIOB
#define J1_B6_Pin GPIO_PIN_15
#define J1_B6_GPIO_Port GPIOB
#define J1_B7_Pin GPIO_PIN_8
#define J1_B7_GPIO_Port GPIOA
#define J1_B8_Pin GPIO_PIN_9
#define J1_B8_GPIO_Port GPIOA
#define J1_B9_Pin GPIO_PIN_10
#define J1_B9_GPIO_Port GPIOA
#define J1_B10_Pin GPIO_PIN_15
#define J1_B10_GPIO_Port GPIOA
#define J1_B11_Pin GPIO_PIN_3
#define J1_B11_GPIO_Port GPIOB
#define J1_B12_Pin GPIO_PIN_4
#define J1_B12_GPIO_Port GPIOB
#define J1_B13_Pin GPIO_PIN_5
#define J1_B13_GPIO_Port GPIOB
#define J1_B14_Pin GPIO_PIN_6
#define J1_B14_GPIO_Port GPIOB
#define J1_B15_Pin GPIO_PIN_7
#define J1_B15_GPIO_Port GPIOB
#define J1_B16_Pin GPIO_PIN_8
#define J1_B16_GPIO_Port GPIOB
#define J1_B17_Pin GPIO_PIN_9
#define J1_B17_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
